/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: selver <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/12 12:23:51 by selver            #+#    #+#             */
/*   Updated: 2022/03/28 10:01:47 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include.h"
//debile

double roundd(double n, double i)
{
    return floor(pow(10,i)*n)/pow(10,i);
}

int vector(void)
{
	return (1);
}

int matrix(void)
{
	t_matrix_float u = matrix_create_from_literal(float, 2, {
			{1., 0.},
			{0., 1.}}
			);

	PRESENT_MATRIX(float, u);

	ASSERT_EQUAL_DOUBLE(matrix_trace_float(u), 2.);

	matrix_free_float(u);

	u = matrix_create_from_literal(float, 3, {
			{2., -5., 0.},
			{4., 3., 7.},
			{-2., 3., 4.}}
			);

	PRESENT_MATRIX(float, u);

	ASSERT_EQUAL_DOUBLE(matrix_trace_float(u), 9.);

	matrix_free_float(u);

	u = matrix_create_from_literal(float, 3, {
			{-2., -8., 4.},
			{1., -23., 4.},
			{0., 6., 4.}}
			);

	PRESENT_MATRIX(float, u);

	ASSERT_EQUAL_DOUBLE(matrix_trace_float(u), -21.);

	matrix_free_float(u);
	return (1);
}

int subject(void)
{
	return vector() && matrix();
}

int main(void)
{
	return !subject();
}
