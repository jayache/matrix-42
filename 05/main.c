/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: selver <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/12 12:23:51 by selver            #+#    #+#             */
/*   Updated: 2022/03/25 12:04:12 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include.h"
//debile

double roundd(double n, double i)
{
    return floor(pow(10,i)*n)/pow(10,i);
}

int vector(void)
{
	t_vector_float u = vector_create_from_literal(float, {1., 0.});
	t_vector_float v = vector_create_from_literal(float, {1., 0.});

	PRESENT_VECTOR(float, u);
	PRESENT_VECTOR(float, v);

	ASSERT_EQUAL(vector_cos_float(u, v), 1.)

	vector_free_float(u);
	vector_free_float(v);

	u = vector_create_from_literal(float, {1., 0.});
	v = vector_create_from_literal(float, {0., 1.});

	PRESENT_VECTOR(float, u);
	PRESENT_VECTOR(float, v);

	ASSERT_EQUAL(vector_cos_float(u, v), 0.)

	vector_free_float(u);
	vector_free_float(v);
	u = vector_create_from_literal(float, {-1., 1.});
	v = vector_create_from_literal(float, {1., -1.});

	PRESENT_VECTOR(float, u);
	PRESENT_VECTOR(float, v);

	ASSERT_EQUAL_DOUBLE(vector_cos_float(u, v), -1.0)

	vector_free_float(u);
	vector_free_float(v);

	u = vector_create_from_literal(float, {2., 1.});
	v = vector_create_from_literal(float, {4., 2.});

	PRESENT_VECTOR(float, u);
	PRESENT_VECTOR(float, v);

	ASSERT_EQUAL_DOUBLE(vector_cos_float(u, v), 1.)

	vector_free_float(u);
	vector_free_float(v);

	u = vector_create_from_literal(float, {1., 2., 3.});
	v = vector_create_from_literal(float, {4., 5., 6.});

	PRESENT_VECTOR(float, u);
	PRESENT_VECTOR(float, v);

	ASSERT_EQUAL_DOUBLE(vector_cos_float(u, v), 0.974631846)

	vector_free_float(u);
	vector_free_float(v);

	return (1);
}

int matrix(void)
{
	return (1);
}

int subject(void)
{
	return vector() && matrix();
}

int main(void)
{
	return !subject();
}
