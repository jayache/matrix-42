/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: selver <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/12 12:23:51 by selver            #+#    #+#             */
/*   Updated: 2022/04/26 09:20:24 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include.h"
//debile

double roundd(double n, double i)
{
    return floor(pow(10,i)*n)/pow(10,i);
}

int vector(void)
{
	return (1);
}

int matrix(void)
{
	t_matrix_float u = matrix_create_from_literal(float, 3, {
			{1., 0., 0.},
			{0., 1., 0.},
			{0., 0., 1.}}
			);

	PRESENT_MATRIX(float, u);

	ASSERT_MATRIX(float, matrix_rref_float(u), 3, {{1., 0., 0.}, {0., 1., 0.}, {0., 0., 1.}});

	matrix_free_float(u);

	u = matrix_create_from_literal(float, 2, {
			{1., 2.},
			{3., 4.}}
			);

	PRESENT_MATRIX(float, u);

	ASSERT_MATRIX(float, matrix_rref_float(u), 2, {{1., 0.}, {0., 1.}});

	matrix_free_float(u);

	u = matrix_create_from_literal(float, 2, {
			{1., 2.},
			{2., 4.}}
			);

	PRESENT_MATRIX(float, u);

	ASSERT_MATRIX(float, matrix_rref_float(u), 2, {{1., 2.}, {0., 0.}});

	matrix_free_float(u);

	u = matrix_create_from_literal(float, 5, {
			{8., 5., -2., 4., 28.},
			{4., 2.5, 20., 4., -4.},
			{8., 5., 1., 4., 17.}}
			);

	PRESENT_MATRIX(float, u);

	ASSERT_MATRIX(float, matrix_rref_float(u), 5, {{1., 0.625, 0., 0., -12.1666667}, {0., 0., 1., 0., -3.6666667}, {0., 0., 0., 1., 29.5}});

	matrix_free_float(u);
	return (1);
}

int subject(void)
{
	return vector() && matrix();
}

int main(void)
{
	return !subject();
}
