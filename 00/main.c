/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: selver <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/12 12:23:51 by selver            #+#    #+#             */
/*   Updated: 2022/03/17 09:41:15 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include.h"


int vector(void)
{
	t_vector_float u = vector_create_from_literal(float, {2.0, 3.0});
	t_vector_float v = vector_create_from_literal(float, {5.0, 7.0});

	printf("With u:");
	vector_print_float(u);
	printf("With v:");
	vector_print_float(v);
	ASSERT_VECTOR(float, vector_add_float(u, v), {7., 10.});
	ASSERT_VECTOR(float, vector_sub_float(u, v), {-3., -4.});
	ASSERT_VECTOR(float, vector_scale_float_float(u, 2.), {4., 6.});
	ASSERT_VECTOR(float, vector_scale_float_float(v, 2.), {10., 14.});

	vector_free_float(u);
	vector_free_float(v);
	return (1);
}

int matrix(void)
{
	t_matrix_float u = matrix_create_from_literal(float, 2, {{1.0, 2.0}, {3.0, 4.0}});
	t_matrix_float v = matrix_create_from_literal(float, 2, {{7.0, 4.0}, {-2, 2}});

	printf("With u:");
	matrix_print_float(u);
	printf("With v:");
	matrix_print_float(v);

	ASSERT_MATRIX(float, matrix_add_float(u, v), 2, {{8., 6.}, {1., 6.}});
	ASSERT_MATRIX(float, matrix_sub_float(u, v), 2, {{-6., -2.}, {5., 2.}});
	ASSERT_MATRIX(float, matrix_scale_float_float(u, 2.), 2, {{2., 4.}, {6., 8.}});
	ASSERT_MATRIX(float, matrix_scale_float_float(v, 2.), 2, {{14., 8.}, {-4., 4.}});

	matrix_free_float(u);
	matrix_free_float(v);
	return (1);
}

int subject(void)
{
	vector() && matrix();
}

int test_creation(void)
{
	t_vector_int a;
	t_vector_int b;
	t_vector_int c;
	t_vector_int d;
	int	ary[] = {1, 2, 3, 4, 5, 6, 7};

	a = vector_create_int(7);
	b = vector_create_from(int, ary);
	c = vector_create_from_literal(int, {1, 2, 3, 4, 5, 6, 7});
	d = vector_dup_int(c);
	printf("With vector_create_int: ");
	vector_print_int(a);
	printf("With vector_create_from: ");
	vector_print_int(b);
	printf("With vector_create_from_literal: ");
	vector_print_int(c);
	printf("With vector_dup_int: ");
	vector_print_int(d);
	vector_free_int(a);
	vector_free_int(b);
	vector_free_int(c);
	vector_free_int(d);
}

int main(void)
{
	return !subject();
}
