/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: selver <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/12 12:23:51 by selver            #+#    #+#             */
/*   Updated: 2022/04/02 10:24:36 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include.h"
//debile

float sine(float x)
{
    const float B = 4/M_PI;
    const float C = -4/(M_PI*M_PI);

    float y = B * x + C * x * abs(x);

    //  const float Q = 0.775;
    const float P = 0.225;

    y = P * (y * abs(y) - y) + y;   // Q * y + P * y * abs(y)


    return y;
}

float cosine(float x)
{
    return sine(x + (M_PI / 2));
}

float tangent(float x)
{
	return sine(x) / cosine(x);
}

double roundd(double n, double i)
{
    return floor(pow(10,i)*n)/pow(10,i);
}

int vector(void)
{
	return (1);
}

int matrix(void)
{
	t_matrix_float u = matrix_projection_float(45., 400. / 500., 0.1, 1000.);

	//PRESENT_MATRIX(float, u);

	for (int y = 0; y < 4; ++y) {
		for (int x = 0; x < 4; ++x) {
			printf("%f", u.m[y][x]);
			if (x < 3)
				printf(",");
		}
		printf("\n");
	}

	return (1);
}

int subject(void)
{
	return vector() && matrix();
}

int main(void)
{
	return !subject();
}
