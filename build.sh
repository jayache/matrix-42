set -e
directories="00 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15"
for dir in $directories
do
	gcc $dir/main.c -I inc -o ex$dir -lm -g3 
done
