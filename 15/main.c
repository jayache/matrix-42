/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: selver <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/12 12:23:51 by selver            #+#    #+#             */
/*   Updated: 2022/04/26 09:17:41 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include.h"

typedef struct	s_complex {
	double real;
	double i;
}				t_complex;

t_complex complex_sub(t_complex a, t_complex b)
{
	t_complex c;
	c.real = a.real - b.real;
	c.i = a.i - b.i;
	return (c);
}
t_complex complex_add(t_complex a, t_complex b)
{
	t_complex c;
	c.real = a.real + b.real;
	c.i = a.i + b.i;
	return (c);
}

t_complex complex_scale(t_complex a, double b)
{
	t_complex c;
	c.real = a.real * b;
	c.i = a.i * b;
	return (c);
}

t_complex complex_mult(t_complex a, t_complex b)
{
	t_complex c;
	c.real = a.real * b.real + a.i * b.i;
	c.i = a.real * b.i + b.real * a.i;
	return (c);
}

int	complex_cmp(t_complex a, t_complex b)
{
	return !((a.real == b.real) && (a.i == b.i));
}

IMPLEMENT_IDENTITY(t_complex, ((t_complex){.real = 1., .i = 0.}))
IMPLEMENT_ZERO(t_complex, (t_complex){0})
IMPLEMENT_SCALE(t_complex, (t_complex a) { return a.real * a.i;})
IMPLEMENT_BASICS(t_complex, (t_complex)((t_complex){.real = 1., .i = 0.}));
IMPLEMENT_PRINT_LAMBDA(t_complex, (t_complex a){ printf("%.2f%+.2fi", a.real, a.i); })
IMPLEMENT_SUB(t_complex, complex_sub)
IMPLEMENT_ADD(t_complex, complex_add)
IMPLEMENT_MULT_SCALAR(t_complex, double, complex_scale)
IMPLEMENT_MULT(t_complex, t_complex, complex_mult)
IMPLEMENT_EQUALITY(t_complex, complex_cmp);
IMPLEMENT_LINEAR_COMBINATION(t_complex, double);
IMPLEMENT_LERP(t_complex, double)
IMPLEMENT_NORM_LAMBDA(t_complex, (t_complex a) { return a.real * a.i; });
IMPLEMENT_DOT_PRODUCT(t_complex)
IMPLEMENT_COS(t_complex)
IMPLEMENT_CROSS_PRODUCT(t_complex)
IMPLEMENT_MULT_MATRIX(t_complex, t_complex)
IMPLEMENT_TRACE(t_complex)
IMPLEMENT_TRANSPOSE(t_complex)
IMPLEMENT_RREF(t_complex)
IMPLEMENT_DETERMINANT(t_complex)
IMPLEMENT_INVERSE(t_complex)
IMPLEMENT_RANK(t_complex)

t_complex c(double r, double i)
{
	return (t_complex){.real = r, .i = i};
}
double roundd(double n, double i)
{
    return floor(pow(10,i)*n)/pow(10,i);
}

int vector(void)
{
	return (1);
}

int matrix(void)
{
	t_matrix_t_complex u = matrix_create_t_complex_identity(4, 4);
	t_matrix_t_complex v = matrix_create_t_complex(4, 4);

	PRESENT_MATRIX(t_complex, u);
	PRESENT_MATRIX(t_complex, v);

	ASSERT_EQUAL_MATRIX(t_complex, matrix_sub_t_complex(u, v), u);
	ASSERT_EQUAL_MATRIX(t_complex, matrix_add_t_complex(u, v), u);
	ASSERT_EQUAL_MATRIX(t_complex, matrix_scale_t_complex_double(u, 1.), u);

	ASSERT(matrix_rank_t_complex(u) == 4)

	u = matrix_create_t_complex_identity(2, 2);

	PRESENT_MATRIX(t_complex, u);

	ASSERT(matrix_rank_t_complex(u) == 2);

	matrix_free_t_complex(u);

	u = matrix_create_from_literal(t_complex, 2, {
			{c(1, -2), c(5, 3)},
			{c(6, 1), c(10, 10)}}
			);
	PRESENT_MATRIX(t_complex, u);

	v = matrix_rref_t_complex(u);
	PRESENT_MATRIX(t_complex, v);

	matrix_free_t_complex(u);
	return (1);
}

int subject(void)
{
	return vector() && matrix();
}

int main(void)
{
	return !subject();
}
