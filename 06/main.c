/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: selver <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/12 12:23:51 by selver            #+#    #+#             */
/*   Updated: 2022/03/26 10:01:40 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include.h"
//debile

double roundd(double n, double i)
{
    return floor(pow(10,i)*n)/pow(10,i);
}

int vector(void)
{
	t_vector_float u = vector_create_from_literal(float, {0., 0., 1.});
	t_vector_float v = vector_create_from_literal(float, {1., 0., 0.});

	PRESENT_VECTOR(float, u);
	PRESENT_VECTOR(float, v);

	ASSERT_VECTOR(float, vector_cross_product_float(u, v), {0., 1., 0.});

	vector_free_float(u);
	vector_free_float(v);

	u = vector_create_from_literal(float, {1., 2., 3.});
	v = vector_create_from_literal(float, {4., 5., 6.});

	PRESENT_VECTOR(float, u);
	PRESENT_VECTOR(float, v);

	ASSERT_VECTOR(float, vector_cross_product_float(u, v), {-3., 6., -3.});

	vector_free_float(u);
	vector_free_float(v);

	u = vector_create_from_literal(float, {4., 2., -3.});
	v = vector_create_from_literal(float, {-2., -5., 16.});

	PRESENT_VECTOR(float, u);
	PRESENT_VECTOR(float, v);

	ASSERT_VECTOR(float, vector_cross_product_float(u, v), {17., -58., -16.});

	vector_free_float(u);
	vector_free_float(v);

	return (1);
}

int matrix(void)
{
	return (1);
}

int subject(void)
{
	return vector() && matrix();
}

int main(void)
{
	return !subject();
}
