/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: selver <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/16 14:38:25 by selver            #+#    #+#             */
/*   Updated: 2022/04/05 09:46:33 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TEST_H
#define TEST_H

#include "vector.h"
#include "matrix.h"

int asprintf(char **strp, const char *fmt, ...);

#define RED "\e[31m"
#define GREEN "\e[42m"
#define RESET "\e[0m"

#define ASSERT(condition) if (!(condition)) printf(RED "Failed test: %s\nIn: %s:%d" RESET "\n", #condition, __FILE__, __LINE__); else printf(GREEN "Passed test: %s" RESET "\n", #condition);
#define PRINT_EXEC(code) printf("%s\n", #code); code

#define ASSERT_EQUAL_DOUBLE(expr, result) { \
	char *tmp;\
	char *tmp2;\
	asprintf(&tmp, "%.5f", expr); \
	asprintf(&tmp2, "%.5f", result); \
	if (!strcmp(tmp, tmp2)) \
		printf(GREEN "Passed test: %s with result %s!" RESET "\n", #expr, #result); \
	else {\
		printf(RED "Failed test: %s\nExpected>%s\nGot     >%s" RESET "\n", #expr, #result, tmp); \
		free(tmp); \
		free(tmp2); \
		return 0; \
	}\
	free(tmp); \
	free(tmp2); \
}
#define ASSERT_EQUAL(expr, result) { \
	double tmp = expr; \
	if (tmp == result) \
		printf(GREEN "Passed test: %s with result %s!" RESET "\n", #expr, #result); \
	else {\
		printf(RED "Failed test: %s\nExpected>%s\nGot     >%.19F" RESET "\n", #expr, #result, tmp); \
		return 0; \
	}\
} \
	
#define ASSERT_EQUAL_MATRIX(type, matrixexpr, matrix)\
{ t_matrix_##type _tmp = matrix_create_##type##_copy(matrix); \
  t_matrix_##type _tmp2 = matrixexpr; \
	if (matrix_equal_##type(_tmp2, _tmp)) {\
		printf(GREEN "Passed test: %s! == " #matrix RESET "\n", #matrixexpr); \
	} \
	else { \
		printf(RED "Failed test: %s\nExpected>", #matrixexpr); \
		matrix_print_##type(_tmp); \
		printf("Got     >"); \
		matrix_print_##type(_tmp2); \
		printf(RESET "\n"); \
		return 0; \
	} \
	matrix_free_##type(_tmp); \
	matrix_free_##type(_tmp2); }\

#define ASSERT_MATRIX(type, matrixexpr, width, ...) \
{ t_matrix_##type _tmp = matrix_create_from_literal(type, width, __VA_ARGS__); \
  t_matrix_##type _tmp2 = matrixexpr; \
	if (matrix_equal_##type(_tmp2, _tmp)) {\
		printf(GREEN "Passed test: %s with result %s!" RESET "\n", #matrixexpr, #__VA_ARGS__); \
	} \
	else { \
		printf(RED "Failed test: %s\nExpected>", #matrixexpr); \
		matrix_print_##type(_tmp); \
		printf("Got     >"); \
		matrix_print_##type(_tmp2); \
		printf(RESET "\n"); \
		return 0; \
	} \
	matrix_free_##type(_tmp); \
	matrix_free_##type(_tmp2); }\

#define ASSERT_VECTOR(type, vectorexpr, ...) \
{ t_vector_##type _tmp = vector_create_from_literal(type, __VA_ARGS__); \
  t_vector_##type _tmp2 = vectorexpr; \
	if (vector_equal_##type(_tmp2, _tmp)) {\
		printf(GREEN "Passed test: %s with result %s!" RESET "\n", #vectorexpr, #__VA_ARGS__); \
	} \
	else { \
		printf(RED "Failed test: %s\nExpected>", #vectorexpr); \
		vector_print_##type(_tmp); \
		printf("Got     >"); \
		vector_print_##type(_tmp2); \
		printf(RESET "\n"); \
		return 0; \
	} \
	vector_free_##type(_tmp); \
	vector_free_##type(_tmp2); }\

#define PRESENT_VECTOR(type, var) printf("With %s:", #var); vector_print_##type(var); 
#define PRESENT_MATRIX(type, var) printf("With %s:", #var); matrix_print_##type(var); 

#endif
