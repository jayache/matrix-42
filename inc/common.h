/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   common.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: selver <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/14 13:40:51 by selver            #+#    #+#             */
/*   Updated: 2022/03/24 10:56:33 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef COMMON_H
#define COMMON_H

#include <strings.h>
#include <stdlib.h>
#include <math.h>

#define ARRAY_SIZE(ary) (sizeof(ary) / sizeof(ary[0]))
#define TAKE_SECOND(_1, _2, _3, ...) _2
#define TAKE_THIRD(_1, _2, _3, ...) _3

#endif
