/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   include.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jayache <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/17 08:54:31 by jayache           #+#    #+#             */
/*   Updated: 2022/04/24 09:19:35 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef INCLUDE_H
#define INCLUDE_H

#include "vector.h"
#include "matrix.h"
#include "other.h"
#include "test.h"

IMPLEMENT_ZERO(int, 0)
IMPLEMENT_IDENTITY(int, 1)
IMPLEMENT_SCALE(int, (int a) { return a; });
IMPLEMENT_BASICS(int, 1)
IMPLEMENT_PRINT_LAMBDA(int, (int a) { printf("%d", a); })
IMPLEMENT_SUB_LAMBDA(int, (int a, int b) { return a - b; })
IMPLEMENT_ADD_LAMBDA(int, (int a, int b) { return a + b; })
IMPLEMENT_MULT_SCALAR_LAMBDA(int, int, (int a, int b) { return a * b; })
IMPLEMENT_MULT_SCALAR_LAMBDA(int, double, (int a, double b) { return (int)(a * b); })
IMPLEMENT_EQUALITY_LAMBDA(int, (int a, int b) { return a - b; })
IMPLEMENT_LINEAR_COMBINATION(int, int)
IMPLEMENT_LERP(int, int)

IMPLEMENT_ZERO(float, 0.)
IMPLEMENT_IDENTITY(float, 1.)
IMPLEMENT_SCALE(float, (float a) { return a; });
IMPLEMENT_BASICS(float, 1.)
IMPLEMENT_PRINT_LAMBDA(float, (float a) { printf("%+.8f", a); })
IMPLEMENT_SUB_LAMBDA(float, (float a, float b) { return a - b; })
IMPLEMENT_ADD_LAMBDA(float, (float a, float b) { return a + b; })
IMPLEMENT_MULT_SCALAR_LAMBDA(float, float, (float a, float b) { return a * b; })
IMPLEMENT_MULT_SCALAR_LAMBDA(float, double, (float a, double b) { return a * b; })
IMPLEMENT_EQUALITY_LAMBDA(float, (float a, float b) { return !(a == b); })
IMPLEMENT_LINEAR_COMBINATION(float, float)
IMPLEMENT_LERP(float, float)
IMPLEMENT_NORM_LAMBDA(float, (float a) { return (double)abs(a); });
IMPLEMENT_DOT_PRODUCT(float);
IMPLEMENT_COS(float);
IMPLEMENT_CROSS_PRODUCT(float)
IMPLEMENT_MULT_MATRIX(float, float)
IMPLEMENT_TRACE(float)
IMPLEMENT_TRANSPOSE(float)
IMPLEMENT_RREF(float)
IMPLEMENT_DETERMINANT(float)
IMPLEMENT_INVERSE(float)
IMPLEMENT_RANK(float)
IMPLEMENT_PROJECTION(float)

t_matrix_float matrix_projection_float(float fov, float ratio, float near, float far);

#endif
