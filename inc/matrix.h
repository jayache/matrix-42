/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   matrix.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: selver <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/08 20:53:24 by selver            #+#    #+#             */
/*   Updated: 2022/04/28 09:46:34 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MATRIX_H
#define MATRIX_H

#include "common.h"

#define _MATRIX_OP_TYPEDEF(type) typedef  type (*matrix_op_##type)(type, type)
#define _MATRIX_STRUCT(type) \
	typedef struct s_matrix_##type {\
		union {\
		type		**matrix;\
		type		**mat;\
		type		**m;\
		};\
		int			width; \
		int			height; \
	}				t_matrix_##type

#define _MATRIX_CREATE(type, identity) \
	t_matrix_##type matrix_create_##type(int width, int height) {\
	t_matrix_##type matrix; \
    int x = 0; \
    matrix.height = -1; \
	if (!(matrix.matrix = malloc(sizeof(type*) * height + 1))) \
		return (matrix); \
        while (x < height) \
        { \
            if (!(matrix.matrix[x] = malloc(sizeof(type) * width + 1))) \
                return (matrix); \
            bzero(matrix.matrix[x], width * sizeof(type)); \
            ++x; }\
        matrix.height = height; \
        matrix.width = width;\
        return (matrix); \
    }  \
	t_matrix_##type matrix_create_##type ##_from(int width, int height, type array[height][width]) {\
		t_matrix_##type matrix = matrix_create_##type(width, height); \
		for (int y = 0; y < height; ++y) \
			for (int x = 0; x < width; ++x) \
				matrix.matrix[y][x] = array[y][x]; \
        return (matrix); \
    }\
	t_matrix_##type matrix_create_##type ##_copy(t_matrix_##type src) {\
		t_matrix_##type matrix = matrix_create_##type(src.width, src.height); \
		for (int y = 0; y < src.height; ++y) \
			for (int x = 0; x < src.width; ++x) \
				matrix.matrix[y][x] = src.matrix[y][x]; \
        return (matrix); \
    }\
	t_matrix_##type matrix_create_##type ##_identity(int width, int height) {\
		t_matrix_##type matrix = matrix_create_##type(width, height); \
		for (int y = 0; y < height; ++y) \
			matrix.matrix[y][y] = (identity);\
        return (matrix); \
    }\


#define matrix_create_from(type, width, ary) \
	matrix_create_##type ##_from(width, ARRAY_SIZE(((type[][width])ary)), ary)
#define matrix_create_from_literal(type, width, ...) \
	matrix_create_##type ##_from(width, ARRAY_SIZE(((type[][width])__VA_ARGS__)), ((type[][width])__VA_ARGS__))

#define _MATRIX_FREE(type) \
	void matrix_free_##type(t_matrix_##type matrix) { \
		for (int i = 0; i < matrix.height; ++i) \
			free(matrix.matrix[i]); \
		free(matrix.matrix); \
		matrix.matrix = NULL; \
		matrix.height = -1; \
		matrix.width = -1; \
	}
#define _MATRIX_FREE_F(type, ffree) \
	void matrix_free_##type(t_matrix_##type matrix) { \
		for (int i = 0; i < matrix.height; ++i) { \
			for (int j = 0; j < matrix.width; ++j) \
				ffree(matrix.matrix[i][j]); \
			free(matrix.matrix[i]); \
		} \
		free(matrix.matrix); \
		matrix.matrix = NULL; \
		matrix.height = -1; \
		matrix.width = -1; \
	}

#define _MATRIX_IMPLEMENT_EQUALITY(type, fcmp) \
	int matrix_equal_##type(t_matrix_##type a, t_matrix_##type b) { \
		if (a.width != b.width || a.height != b.height) \
			return 0; \
		for (int i = 0; i < a.height; ++i) \
			for (int j = 0; j < a.width; ++j) \
				if (fcmp(a.matrix[i][j], b.matrix[i][j])) \
					return 0; \
		return 1; \
	}

#define _MATRIX_IMPLEMENT_PRINT(type, fprintmat) \
	void matrix_print_##type(t_matrix_##type mat) { \
		printf("Matrix(%d, %d) (" #type ")\n[\n", mat.width, mat.height); \
		for (int i = 0; i < mat.height; ++i) {\
			printf(" ["); \
			for (int j = 0; j < mat.width; ++j) { \
			fprintmat(mat.matrix[i][j]); \
			if (j < mat.width - 1) \
				printf(","); \
			} \
			printf("]\n"); \
		} \
		printf("]\n"); \
	}

#define _MATRIX_IMPLEMENT_ADD(type, function) \
	void matrix_add_ ##type ##_mut(t_matrix_##type a, t_matrix_##type b) { \
		for (int i = 0; i < a.height; ++i) \
			for (int j = 0; j < a.width; ++j) \
				a.matrix[i][j] = function(a.matrix[i][j], b.matrix[i][j]); \
	} \
	t_matrix_##type matrix_add_ ##type (t_matrix_##type a, t_matrix_##type b) { \
		t_matrix_##type c = matrix_create_##type(a.width, a.height); \
		for (int i = 0; i < a.height; ++i) \
			for (int j = 0; j < a.width; ++j) \
				c.matrix[i][j] = function(a.matrix[i][j], b.matrix[i][j]); \
		return (c); \
	}

#define _MATRIX_IMPLEMENT_SUB(type, function) \
	void matrix_sub_ ##type ##_mut(t_matrix_##type a, t_matrix_##type b) { \
		for (int i = 0; i < a.height; ++i) \
			for (int j = 0; j < a.width; ++j) \
				a.matrix[i][j] = function(a.matrix[i][j], b.matrix[i][j]); \
	} \
	t_matrix_##type matrix_sub_ ##type(t_matrix_##type a, t_matrix_##type b) { \
		t_matrix_##type c = matrix_create_##type(a.width, a.height); \
		for (int i = 0; i < a.height; ++i) \
			for (int j = 0; j < a.width; ++j) \
				c.matrix[i][j] = function(a.matrix[i][j], b.matrix[i][j]); \
		return (c); \
	}

#define _MATRIX_IMPLEMENT_MULT_SCALAR(typem, types, function) \
	void matrix_scale_##typem ##_##types ##_mut(t_matrix_##typem a, types b) { \
		for (int i = 0; i < a.height; ++i) \
			for (int j = 0; j < a.width; ++j) \
				a.matrix[i][j] = function(a.matrix[i][j], b); \
	} \
	t_matrix_##typem matrix_scale_##typem ##_##types (t_matrix_##typem a, types b) { \
		t_matrix_##typem c = matrix_create_##typem(a.width, a.height); \
		for (int i = 0; i < a.height; ++i) \
			for (int j = 0; j < a.width; ++j) \
				c.matrix[i][j] = function(a.matrix[i][j], b); \
		return (c); \
	}

#define _MATRIX_IMPLEMENT_LINEAR_COMBINATION(typem, types) \
	t_matrix_##typem matrix_linear_combination_##typem ##_## types (int size, t_matrix_##typem matrices[size], types scalars[size]) { \
		t_matrix_##typem c = matrix_create_##typem(matrices[0].height, matrices[0].width); \
		for (int i = 0; i < size; ++i){ \
			t_matrix_##typem tmp = matrix_scale_##typem ##_ ##types(matrices[i], scalars[i]); \
			matrix_add_##typem ##_mut(c, tmp); \
			matrix_free_##typem(tmp); \
		} \
		return (c); \
	}

#define _MATRIX_IMPLEMENT_LERP(typem, types) \
	t_matrix_##typem matrix_lerp_##typem ##_##types(t_matrix_##typem begin, t_matrix_##typem end, types point) { \
		t_matrix_##typem tmp = matrix_sub_##typem(end, begin);\
		matrix_scale_##typem ##_##types ##_mut(tmp, point); \
		matrix_add_##typem ##_mut(tmp, begin); \
		return (tmp); \
	}

#define _MATRIX_MULT_MATRIX(type) \
	t_matrix_##type matrix_mult_ ##type(t_matrix_##type a, t_matrix_##type b) { \
		t_matrix_##type tmp = matrix_create_##type(a.height, b.width);\
		for (int i = 0; i < a.height; ++i) { \
			for (int k = 0; k < b.width; ++k) { \
				for (int j = 0; j < a.width; ++j) { \
					tmp.matrix[i][k] = __add_##type (tmp.matrix[i][k], __mult_##type ##_## type (a.matrix[i][j], b.matrix[j][k])); \
				}\
			} \
		}\
		return (tmp); \
	}

#define _MATRIX_TRACE(type) \
	type matrix_trace_##type (t_matrix_##type a) { \
		type tmp = {0}; \
		for (int i = 0; i < a.width; ++i) { \
			tmp = __add_##type (tmp, a.matrix[i][i]); \
		}\
		return (tmp); \
	}

#define _MATRIX_TRANSPOSE(type) \
	t_matrix_##type matrix_transpose_##type(t_matrix_##type a) { \
		t_matrix_##type tmp = matrix_create_##type(a.height, a.width); \
		for (int i = 0; i < a.width; ++i) { \
			for (int j = 0; j < a.height; ++j) { \
				tmp.matrix[i][j] = a.matrix[j][i];\
			}\
		}\
		return (tmp);\
	}

#define _MATRIX_RREF(type) \
	void	__swap_rows_##type(int width, type *a, type *b) {\
		type tmp[width];\
		memset(tmp, 0, width * sizeof(type));\
		memmove(tmp, a, width * sizeof(type));\
		memmove(a, b, width * sizeof(type));\
		memmove(b, tmp, width * sizeof(type));\
	}\
	void	__scalar_mult_row_##type(int width, type *row, double scalar) {\
		for (int i = 0; i < width; ++i) \
			row[i] = __mult_##type##_double(row[i], scalar);\
	}\
	void	__row_sum_##type(int width, type *target, type *source, double scalar) {\
		for (int i = 0; i < width; ++i) \
			target[i] = __add_##type(target[i], __mult_##type##_double(source[i], scalar));\
	}\
	void	__actual_rref_##type(int width, int height, type **array) { \
		int nonempty;\
		int	minrow = 0;\
		for (int i = 0; i < width; ++i) {\
			nonempty = -1;\
			for (nonempty = minrow; nonempty < height && !__cmp_##type(array[nonempty][i], __zero_##type()); ++nonempty);\
			if (nonempty == height)\
				continue;\
			if (nonempty != minrow)\
				__swap_rows_##type(width, array[nonempty], array[minrow]);\
			__scalar_mult_row_##type(width - i, array[minrow] + i, 1.0 / (double)__scalar_##type(array[minrow][i]));\
			for (int x = 0; x < height; ++x)\
				if (x != minrow) \
					__row_sum_##type(width - i, array[x] + i, array[minrow] + i, -__scalar_##type (array[x][i]));\
			minrow++;\
		}\
	}\
	t_matrix_##type matrix_rref_##type(t_matrix_##type a) { \
		t_matrix_##type tmp = matrix_create_##type ##_copy(a);\
		__actual_rref_##type(a.width, a.height, tmp.matrix);\
		return (tmp);\
	}\

#define _MATRIX_DETERMINANT(type) \
	type __determinant_##type##_2(type a, type b, type c, type d) { \
		type tmp = __mult_##type ##_##type(a, d);\
		type tmp2 = __mult_##type ##_##type(c, b);\
		return __sub_##type (tmp, tmp2); \
	}\
	type __determinant_##type##_3(type a, type b, type c, type d, type e, type f, type g, type h, type i) { \
		type tmp = __mult_##type##_##type(a, __determinant_##type##_2(e, f, h, i));\
		type tmp2= __mult_##type##_##type(b, __determinant_##type##_2(d, f, g, i));\
		type tmp3= __mult_##type##_##type(c, __determinant_##type##_2(d, e, g, h));\
		return __add_##type (__sub_##type (tmp, tmp2), tmp3);\
	}\
	type __determinant_##type##_4(type a, type b, type c, type d, type e, type f, type g, type h, type i, type j, type k, type l, type m, type n, type o, type p) { \
		type tmp = __mult_##type##_##type(a, __determinant_##type##_3(f, g, h, j, k, l, n, o, p));\
		type tmp2= __mult_##type##_##type(b, __determinant_##type##_3(e, g, h, i, k, l, m, o, p));\
		type tmp3= __mult_##type##_##type(c, __determinant_##type##_3(e, f, h, i, j, l, m, n, p));\
		type tmp4= __mult_##type##_##type(d, __determinant_##type##_3(e, f, g, i, j, k, m, n, o));\
		return __sub_##type(__add_##type (__sub_##type (tmp, tmp2), tmp3), tmp4);\
	}\
	type matrix_determinant_##type(t_matrix_##type a) { \
		if (a.width == 2 && a.height == 2) \
			return __determinant_##type##_2(a.m[0][0], a.m[0][1], a.m[1][0], a.m[1][1]);\
		if (a.width == 3 && a.height == 3) \
			return __determinant_##type##_3(a.m[0][0], a.m[0][1], a.m[0][2], a.m[1][0], a.m[1][1], a.m[1][2], a.m[2][0], a.m[2][1], a.m[2][2]);\
		if (a.width == 4 && a.height == 4) \
			return __determinant_##type##_4(a.m[0][0], a.m[0][1], a.m[0][2], a.m[0][3], a.m[1][0], a.m[1][1], a.m[1][2], a.m[1][3], a.m[2][0], a.m[2][1], a.m[2][2], a.m[2][3], a.m[3][0], a.m[3][1], a.m[3][2], a.m[3][3]);\
		return (type){0};\
	}

#define _MATRIX_INVERSE(type) \
	void	__rref_##type##_inverse(int width, int height, type **array, type **identity) { \
		int nonempty;\
		int	minrow = 0;\
		for (int i = 0; i < width; ++i) {\
			nonempty = -1;\
			for (nonempty = minrow; nonempty < height && !__cmp_##type(array[nonempty][i], __zero_##type()); ++nonempty);\
			if (nonempty == height)\
				continue;\
			if (nonempty != minrow) {\
				__swap_rows_##type(width, identity[nonempty], identity[minrow]);\
				__swap_rows_##type(width, array[nonempty], array[minrow]);\
			}\
			__scalar_mult_row_##type(width, identity[minrow], 1.0 / __scalar_##type(array[minrow][i]));\
			__scalar_mult_row_##type(width - i, array[minrow] + i, 1.0 / __scalar_##type(array[minrow][i]));\
			for (int x = 0; x < height; ++x)\
				if (x != minrow) {\
					__row_sum_##type(width, identity[x], identity[minrow], -__scalar_##type(array[x][i]));\
					__row_sum_##type(width, array[x], array[minrow], -__scalar_##type(array[x][i]));\
				}\
			minrow++;\
		}\
	}\
	t_matrix_##type matrix_inverse_##type (t_matrix_##type a) {\
		t_matrix_##type tmp = matrix_create_##type ##_copy(a);\
		t_matrix_##type identity = matrix_create_##type ##_identity(a.width, a.height);\
		__rref_##type##_inverse(a.width, a.height, tmp.m, identity.m);\
		return (identity);\
	}

#define _MATRIX_RANK(type) \
	int	matrix_rank_##type(t_matrix_##type a) { \
		int rank = 0;\
		t_matrix_##type tmp = matrix_rref_##type(a);\
		/*counts nonzero row*/\
		for (int i = 0; i < a.height; ++i) {\
			int nonzero = 0;\
			for (int j = 0; j < a.width; ++j) {\
				nonzero |= (__cmp_##type(tmp.m[i][j], __zero_##type()));\
			}\
			rank += nonzero;\
		}\
		matrix_free_##type(tmp);\
		return (rank);\
	}

#define _MATRIX_PROJECTION(type)\
	float sine_##type(float x)\
	{\
		const float B = 4/M_PI;\
		const float C = -4/(M_PI*M_PI);\
		float y = B * x + C * x * abs(x);\
		const float P = 0.225;\
		y = P * (y * abs(y) - y) + y;\
		return y;\
	}\
	float cosine_##type(float x)\
	{\
		return sine_##type(x + (M_PI / 2));\
	}\
	float tangent_##type(float x)\
	{\
		return sine_##type(x) / cosine_##type(x);\
	}\
	t_matrix_##type matrix_projection_##type (type fov, type ratio, type near, type far) {\
		t_matrix_##type	matrix;\
		float		right;\
		float		left;\
		float		top;\
		float		bottom;\
		top = near * tangent_##type((fov * M_PI / 180.) / 2);\
		right = top * ratio;\
		left = -right;\
		bottom = -top;\
		matrix = matrix_create_##type(4, 4);\
		matrix.m[0][0] = 2 * near / (right - left);\
		matrix.m[1][1] = 2 * near / (top - bottom);\
		matrix.m[2][2] = -(far + near) / (far - near);\
		matrix.m[2][3] = -1;\
		matrix.m[3][2] = -2 * far * near / (far - near);\
		return (matrix);\
	}

#define _MATRIX_CREATE_NO_F(type) _MATRIX_CREATE(type, 1.)

#define CHOOSE_CREATE(...) \
	TAKE_THIRD(__VA_ARGS__, _MATRIX_CREATE, _MATRIX_CREATE_NO_F, BAD, BAD)

#define FIRST(a, ...) a
#define SECOND(a, b, ...) b

#define _DEFINE_MATRIX(type, identity)\
	_MATRIX_STRUCT(type);\
	_MATRIX_CREATE(type, identity);\
	_MATRIX_FREE(type)
#endif
