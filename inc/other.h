/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   other.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: selver <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/17 14:12:09 by selver            #+#    #+#             */
/*   Updated: 2022/04/24 09:20:13 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef OTHER_H
#define OTHER_H

#include "vector.h"
#include "matrix.h"

#define IMPLEMENT_BASICS(type, ...) \
	_DEFINE_VECTOR(type) \
	_DEFINE_MATRIX(type, __VA_ARGS__)

#define IMPLEMENT_ZERO(type, zero) \
	type __zero_##type() { return zero; }

#define IMPLEMENT_SCALE(type, body) \
	double	__scalar_##type body \

#define IMPLEMENT_IDENTITY(type, id) \
	type __identity_##type() { return id; }

#define IMPLEMENT_PRINT(type, fprint) \
	_VECTOR_IMPLEMENT_PRINT(type, fprint) \
	_MATRIX_IMPLEMENT_PRINT(type, fprint)

#define IMPLEMENT_PRINT_LAMBDA(type, body) \
	void	__print_##type body \
	IMPLEMENT_PRINT(type, __print_##type)

#define IMPLEMENT_EQUALITY(type, fcmp) \
	int __cmp_##type (t_complex a, t_complex b) { return fcmp(a, b); }\
	_VECTOR_IMPLEMENT_EQUALITY(type, fcmp) \
	_MATRIX_IMPLEMENT_EQUALITY(type, fcmp)

#define IMPLEMENT_EQUALITY_LAMBDA(type, body) \
	int __cmp_##type body \
	_VECTOR_IMPLEMENT_EQUALITY(type, __cmp_##type) \
	_MATRIX_IMPLEMENT_EQUALITY(type, __cmp_##type)

#define IMPLEMENT_SUB(type, fsub) \
	type __sub_##type (type a, type b) { return fsub(a, b); } \
	_VECTOR_IMPLEMENT_SUB(type, fsub) \
	_MATRIX_IMPLEMENT_SUB(type, fsub)

#define IMPLEMENT_SUB_LAMBDA(type, body) \
	type	__sub_##type body \
	_VECTOR_IMPLEMENT_SUB(type, __sub_##type) \
	_MATRIX_IMPLEMENT_SUB(type, __sub_##type) \

#define IMPLEMENT_ADD(type, fadd) \
	type __add_##type (type a, type b) { return fadd(a, b); }\
	_VECTOR_IMPLEMENT_ADD(type, fadd) \
	_MATRIX_IMPLEMENT_ADD(type, fadd)

#define IMPLEMENT_ADD_LAMBDA(type, body) \
	type	__add_##type body \
	_VECTOR_IMPLEMENT_ADD(type, __add_##type) \
	_MATRIX_IMPLEMENT_ADD(type, __add_##type)

#define IMPLEMENT_MULT_SCALAR(typev, types, fmult) \
	typev	__mult_##typev ##_##types (typev a, types b) { return fmult(a, b); }\
	_VECTOR_IMPLEMENT_MULT_SCALAR(typev, types, fmult) \
	_MATRIX_IMPLEMENT_MULT_SCALAR(typev, types, fmult)

#define IMPLEMENT_MULT_SCALAR_LAMBDA(typev, types, body) \
	typev	__mult_##typev ##_##types body \
	_VECTOR_IMPLEMENT_MULT_SCALAR(typev, types, __mult_##typev ##_##types) \
	_MATRIX_IMPLEMENT_MULT_SCALAR(typev, types, __mult_##typev ##_##types) \

#define IMPLEMENT_LINEAR_COMBINATION(typev, types) \
	_VECTOR_IMPLEMENT_LINEAR_COMBINATION(typev, types) \
	_MATRIX_IMPLEMENT_LINEAR_COMBINATION(typev, types)

#define IMPLEMENT_LERP(typev, types) \
	typev lerp_##typev(typev begin, typev end, types point) {\
		typev tmp = __sub_##typev(end, begin); \
		tmp = __mult_##typev ##_##types(tmp, point); \
		tmp = __add_##typev(tmp, begin); \
		return (tmp); \
	} \
	_VECTOR_IMPLEMENT_LERP(typev, types) \
	_MATRIX_IMPLEMENT_LERP(typev, types)

#define IMPLEMENT_DOT_PRODUCT(typev) \
	_VECTOR_IMPLEMENT_DOT_PRODUCT(typev)

#define IMPLEMENT_NORM(typev, fabs) \
	_VECTOR_IMPLEMENT_NORM(typev, fabs)

#define IMPLEMENT_NORM_LAMBDA(typev, body) \
	 double __abs_##typev body \
	_VECTOR_IMPLEMENT_NORM(typev, __abs_##typev)

#define IMPLEMENT_COS(typev) \
	_VECTOR_IMPLEMENT_COS(typev)

#define IMPLEMENT_CROSS_PRODUCT(typev) \
	_VECTOR_IMPLEMENT_CROSS_PRODUCT(typev);

#define IMPLEMENT_MULT_MATRIX(typev, typem) \
	_VECTOR_MULT_MATRIX(typev, typem); \
	_MATRIX_MULT_MATRIX(typem);

#define IMPLEMENT_MULT(typev, typem, fmult) \
	typev __mult_##typev ##_##typem (typev a, typem b) { return fmult(a, b); } \
	
#define IMPLEMENT_MULT_LAMBDA(typev, typem, body) \
	typev __mult_##typev ##_##typem body \

#define IMPLEMENT_TRACE(type) \
	_MATRIX_TRACE(type)

#define IMPLEMENT_TRANSPOSE(type) \
	_MATRIX_TRANSPOSE(type)

#define IMPLEMENT_RREF(type) \
	_MATRIX_RREF(type)

#define IMPLEMENT_DETERMINANT(type) \
	_MATRIX_DETERMINANT(type)

#define IMPLEMENT_INVERSE(type)\
	_MATRIX_INVERSE(type)
	
#define IMPLEMENT_RANK(type)\
	_MATRIX_RANK(type)

#define IMPLEMENT_PROJECTION(type)\
	_MATRIX_PROJECTION(type)
#endif
