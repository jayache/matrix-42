/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vector.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: selver <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/09 08:22:26 by selver            #+#    #+#             */
/*   Updated: 2022/04/03 15:13:43 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef VECTOR_H
#define VECTOR_H

#include <strings.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "common.h"


#define _VECTOR_STRUCT(type) \
	typedef struct s_vector_##type { \
		union {\
		type	*vector;\
		type	*vec; \
		type	*v;\
		};\
		int		size; \
	}				t_vector_##type;

#define _VECTOR_CREATE(type) \
	t_vector_##type	vector_create_##type(int size) { \
		t_vector_##type vec; \
		vec.size = -1; \
		if (!(vec.vec = malloc(sizeof(type) * size))) \
			return (vec); \
		bzero(vec.vec, size * sizeof(type)); \
		vec.size = size; \
		return (vec); \
}  \
	t_vector_##type vector_create_##type ##_from(int size, type array[]) { \
		t_vector_##type ret = vector_create_##type(size); \
		for (int i = 0; i < size; ++i) \
			ret.vec[i] = array[i];  \
		return ret; \
	} \
	t_vector_##type vector_dup_##type(t_vector_##type src) {\
		t_vector_##type ret; \
		ret.size = -1; \
		if (!(ret.vec = malloc(sizeof(type) * src.size))) \
			return (ret); \
		memcpy(ret.vec, src.vec, src.size * sizeof(type)); \
		ret.size = src.size; \
		return (ret); \
	}

#define _VECTOR_IMPLEMENT_EQUALITY(type, fcmp) \
	int	vector_equal_##type(t_vector_##type a, t_vector_##type b) { \
		if (a.size != b.size) \
			return 0; \
		for (int i = 0; i < a.size; ++i) \
			if (fcmp(a.vec[i], b.vec[i])) \
				return 0; \
		return 1; \
	}

#define vector_create_from(type, ary)\
	vector_create_##type ##_from(ARRAY_SIZE(ary), ary)

#define vector_create_from_literal(type, ...)\
	vector_create_##type ##_from(ARRAY_SIZE(((type[])__VA_ARGS__)), (type[])__VA_ARGS__)

#define _VECTOR_FREE(type) \
	void vector_free_##type(t_vector_##type vec) { \
		free(vec.vec);  \
		vec.size = -1; \
	}

#define _VECTOR_IMPLEMENT_PRINT(type, fprintvec) \
	void vector_print_##type(t_vector_##type vec) { \
		printf("Vector%d ("#type") [", vec.size); \
		for (int i = 0; i < vec.size; ++i) {\
			fprintvec(vec.vec[i]); \
			if (i < vec.size - 1) \
				printf(","); \
		} \
		printf("]\n"); \
	}

#define _VECTOR_IMPLEMENT_ADD(type, fprintadd) \
	void	vector_add_##type ##_mut(t_vector_##type a, t_vector_##type b) { \
		for (int i = 0; i < a.size; ++i) \
		a.vec[i] = fprintadd(a.vec[i], b.vec[i]); \
	} \
	t_vector_##type vector_add_##type(t_vector_##type a, t_vector_##type b) { \
		t_vector_##type c = vector_create_##type(a.size); \
		for (int i = 0; i < a.size; ++i) \
			c.vec[i] = fprintadd(a.vec[i], b.vec[i]); \
		return (c);\
	}

#define _VECTOR_IMPLEMENT_MULT_SCALAR(typev, types, fmult) \
	void	vector_scale_##typev ##_##types ##_mut(t_vector_##typev a, types b) { \
		for (int i = 0; i < a.size; ++i) \
		a.vec[i] = fmult(a.vec[i], b); \
	}\
	t_vector_##typev vector_scale_##typev ##_##types(t_vector_##typev a, types b) { \
		t_vector_##typev c = vector_create_##typev(a.size); \
		for (int i = 0; i < a.size; ++i) \
			c.vec[i] = fmult(a.vec[i], b); \
		return (c);\
	}

#define _VECTOR_IMPLEMENT_SUB(type, fprintsub) \
	void	vector_sub_##type ##_mut(t_vector_##type a, t_vector_##type b) { \
		for (int i = 0; i < a.size; ++i) \
			a.vec[i] = fprintsub(a.vec[i], b.vec[i]); \
	} \
	t_vector_##type vector_sub_##type(t_vector_##type a, t_vector_##type b) { \
		t_vector_##type c = vector_create_##type(a.size); \
		for (int i = 0; i < a.size; ++i) \
			c.vec[i] = fprintsub(a.vec[i], b.vec[i]); \
		return (c);\
	}

#define _VECTOR_IMPLEMENT_LINEAR_COMBINATION(typev, types) \
	t_vector_##typev vector_linear_combination_##typev ##_## types (int size, t_vector_##typev vectors[size], types scalars[size]) { \
		t_vector_##typev c = vector_create_##typev(vectors[0].size); \
		for (int i = 0; i < size; ++i){ \
			t_vector_##typev tmp = vector_scale_##typev ##_ ##types(vectors[i], scalars[i]); \
			vector_add_##typev ##_mut(c, tmp); \
			vector_free_##typev(tmp); \
		} \
		return (c); \
	}

#define _VECTOR_IMPLEMENT_LERP(typev, types) \
	t_vector_##typev vector_lerp_##typev ##_##types(t_vector_##typev begin, t_vector_##typev end, types point) { \
		t_vector_##typev tmp = vector_sub_##typev(end, begin);\
		vector_scale_##typev ##_##types ##_mut(tmp, point); \
		vector_add_##typev ##_mut(tmp, begin); \
		return (tmp); \
	}

#define _VECTOR_IMPLEMENT_DOT_PRODUCT(type) \
	type vector_dot_product_##type(t_vector_##type a, t_vector_##type b) { \
		type c = __zero_##type(); \
		type d = __identity_##type(); \
		for (int i = 0; i < a.size; ++i) { \
			d = __mult_##type ##_##type(a.vec[i], b.vec[i]); \
			c = __add_##type(c, d); \
		} \
		return (c); \
	}

#define _VECTOR_IMPLEMENT_NORM(type, fnorm) \
	double vector_norm_##type ##_1(t_vector_##type a) { \
		double ret = 0; \
		for (int i = 0; i < a.size; ++i) \
			ret += abs(fnorm(a.vec[i])); \
		return (ret); \
	} \
	double vector_norm_##type(t_vector_##type a){ \
		double ret = 0; \
		for (int i = 0; i < a.size; ++i) { \
			ret += pow(fnorm(a.vec[i]), 2); \
		} \
		return (pow(ret, 0.5)); \
	} \
	double vector_norm_##type ##_inf(t_vector_##type a) { \
		double ret = 0; \
		for (int i = 0; i < a.size; ++i) { \
			double tmp = fnorm(a.vec[i]); \
			if (tmp > ret) \
				ret = tmp; \
		} \
		return (ret);\
	}

#define _VECTOR_IMPLEMENT_COS(type) \
	type vector_cos_##type (t_vector_##type a, t_vector_##type b) { \
		return __mult_##type ##_double(vector_dot_product_##type (a, b), 1. /\
		(vector_norm_##type(a) * vector_norm_##type(b))); \
	}

#define _VECTOR_IMPLEMENT_CROSS_PRODUCT(type) \
	t_vector_##type	vector_cross_product_##type (t_vector_##type a, t_vector_##type b) { \
		t_vector_##type ret = vector_create_##type(3);\
		ret.vec[0] = __sub_##type (__mult_##type ##_##type (a.vec[1], b.vec[2]), __mult_##type ##_##type (a.vec[2], b.vec[1]));\
		ret.vec[1] = __sub_##type (__mult_##type ##_##type (a.vec[2], b.vec[0]), __mult_##type ##_##type (a.vec[0], b.vec[2]));\
		ret.vec[2] = __sub_##type (__mult_##type ##_##type (a.vec[0], b.vec[1]), __mult_##type ##_##type (a.vec[1], b.vec[0]));\
		return (ret); \
	}

#define _VECTOR_MULT_MATRIX(typev, typem) \
	t_vector_##typev vector_mult_ ##typev ##_matrix_##typem(t_matrix_##typev a, t_vector_##typem b) { \
		t_vector_##typev ret = vector_create_##typev (b.size);\
		for (int i = 0; i < b.size; ++i) { \
			for (int j = 0; j < a.height; ++j) { \
				ret.vec[i] = __add_##typev (ret.vec[i], __mult_##typev ##_## typem (b.vec[j], a.matrix[i][j])); \
			} \
		}\
		return (ret); \
	}

#define _DEFINE_VECTOR(type) \
	_VECTOR_STRUCT(type) \
	_VECTOR_CREATE(type) \
	_VECTOR_FREE(type)

#endif
