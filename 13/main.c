/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: selver <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/12 12:23:51 by selver            #+#    #+#             */
/*   Updated: 2022/03/30 11:05:23 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include.h"
//debile

double roundd(double n, double i)
{
    return floor(pow(10,i)*n)/pow(10,i);
}

int vector(void)
{
	return (1);
}

int matrix(void)
{
	t_matrix_float u = matrix_create_from_literal(float, 3, {
			{1., 0., 0.},
			{0., 1., 0.},
			{0., 0., 1.}}
			);

	PRESENT_MATRIX(float, u);

	ASSERT_EQUAL(matrix_rank_float(u), 3)

	matrix_free_float(u);

	u = matrix_create_from_literal(float, 4, {
			{1., 2., 0., 0.},
			{2., 4., 0., 0.},
			-1., 2., 1., 1.}
			);

	PRESENT_MATRIX(float, u);

	ASSERT_EQUAL(matrix_rank_float(u), 2)

	matrix_free_float(u);

	u = matrix_create_from_literal(float, 3, {
			{8., 5., -2},
			{4., 7., 20.},
			{7., 6., 1.},
			{21., 18., 7.}}
			);

	PRESENT_MATRIX(float, u);

	ASSERT_EQUAL(matrix_rank_float(u), 3)

	matrix_free_float(u);
	return (1);
}

int subject(void)
{
	return vector() && matrix();
}

int main(void)
{
	return !subject();
}
