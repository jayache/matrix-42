/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: selver <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/12 12:23:51 by selver            #+#    #+#             */
/*   Updated: 2022/03/28 10:36:14 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include.h"
//debile

double roundd(double n, double i)
{
    return floor(pow(10,i)*n)/pow(10,i);
}

int vector(void)
{
	return (1);
}

int matrix(void)
{
	t_matrix_float u = matrix_create_from_literal(float, 1, {
			{1.},
			{0.}}
			);

	PRESENT_MATRIX(float, u);

	ASSERT_MATRIX(float, matrix_transpose_float(u), 2, {{1., 0.}});

	matrix_free_float(u);

	u = matrix_create_from_literal(float, 2, {
			{1., 1.},
			{0., 0.}}
			);

	PRESENT_MATRIX(float, u);

	ASSERT_MATRIX(float, matrix_transpose_float(u), 2, {{1., 0.}, {1., 0.}});

	matrix_free_float(u);
	return (1);
}

int subject(void)
{
	return vector() && matrix();
}

int main(void)
{
	return !subject();
}
