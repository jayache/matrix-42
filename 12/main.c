/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: selver <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/12 12:23:51 by selver            #+#    #+#             */
/*   Updated: 2022/03/30 10:46:11 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include.h"
//debile

double roundd(double n, double i)
{
    return floor(pow(10,i)*n)/pow(10,i);
}

int vector(void)
{
	return (1);
}

int matrix(void)
{
	t_matrix_float u = matrix_create_from_literal(float, 3, {
			{1., 0., 0.},
			{0., 1., 0.},
			{0., 0., 1.}}
			);

	PRESENT_MATRIX(float, u);

	ASSERT_MATRIX(float, matrix_inverse_float(u), 3, {{1., 0., 0.}, {0., 1., 0.}, {0., 0., 1.}});

	matrix_free_float(u);

	u = matrix_create_from_literal(float, 3, {
			{2., 0., 0.},
			{0., 2., 0.},
			{0., 0., 2.}}
			);

	PRESENT_MATRIX(float, u);

	ASSERT_MATRIX(float, matrix_inverse_float(u), 3, {{0.5, 0., 0.}, {0., 0.5, 0.}, {0., 0., 0.5}});

	matrix_free_float(u);
	
	u = matrix_create_from_literal(float, 3, {
			{8., 5., -2.},
			{4., 7., 20.},
			{7., 6., 1.}}
			);

	PRESENT_MATRIX(float, u);

	ASSERT_MATRIX(float, matrix_inverse_float(u), 3, {{0.649425287, 0.097701149, -0.655172414}, {-0.781609195, -0.126436782, 0.965517241}, {0.143678161, 0.074712644, -0.206896552}});

	matrix_free_float(u);
	return (1);
}

int subject(void)
{
	return vector() && matrix();
}

int main(void)
{
	return !subject();
}
