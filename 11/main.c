/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: selver <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/12 12:23:51 by selver            #+#    #+#             */
/*   Updated: 2022/03/30 09:56:35 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include.h"
//debile

double roundd(double n, double i)
{
    return floor(pow(10,i)*n)/pow(10,i);
}

int vector(void)
{
	return (1);
}

int matrix(void)
{
	t_matrix_float u = matrix_create_from_literal(float, 2, {
			{1., -1.},
			{-1., 1.}}
			);

	PRESENT_MATRIX(float, u);

	ASSERT_EQUAL(matrix_determinant_float(u), 0.);

	matrix_free_float(u);

	u = matrix_create_from_literal(float, 3, {
			{2., 0., 0.},
			{0., 2., 0.},
			{0., 0., 2.}}
			);

	PRESENT_MATRIX(float, u);

	ASSERT_EQUAL(matrix_determinant_float(u), 8.);

	matrix_free_float(u);
	
	u = matrix_create_from_literal(float, 3, {
			{8., 5., -2.},
			{4., 7., 20.},
			{7., 6., 1.}}
			);

	PRESENT_MATRIX(float, u);

	ASSERT_EQUAL(matrix_determinant_float(u), -174.);

	matrix_free_float(u);
	u = matrix_create_from_literal(float, 4, {
			{8., 5., -2, 4.},
			{4., 2.5, 20., 4.},
			{8., 5., 1., 4.},
			{28., -4., 17., 1.}}
			);

	PRESENT_MATRIX(float, u);

	ASSERT_EQUAL(matrix_determinant_float(u), 1032.);

	matrix_free_float(u);

	return (1);
}

int subject(void)
{
	return vector() && matrix();
}

int main(void)
{
	return !subject();
}
