/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: selver <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/12 12:23:51 by selver            #+#    #+#             */
/*   Updated: 2022/03/24 11:24:35 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include.h"
//debile

double roundd(double n, double i)
{
    return floor(pow(10,i)*n)/pow(10,i);
}

int vector(void)
{
	t_vector_float u = vector_create_from_literal(float, {0., 0., 0.});

	PRESENT_VECTOR(float, u);

	ASSERT(vector_norm_float_1(u) == 0.)
	ASSERT(vector_norm_float(u) == 0.)
	ASSERT(vector_norm_float_inf(u) == 0.)

	vector_free_float(u);
	u = vector_create_from_literal(float, {1., 2., 3.});

	PRESENT_VECTOR(float, u);

	ASSERT(vector_norm_float_1(u) == 6.)
	ASSERT_EQUAL(roundd(vector_norm_float(u), 8), 3.74165738)
	ASSERT(vector_norm_float_inf(u) == 3.)

	vector_free_float(u);

	u = vector_create_from_literal(float, {-1., -2.});

	PRESENT_VECTOR(float, u);

	ASSERT(vector_norm_float_1(u) == 3.)
	ASSERT_EQUAL(roundd(vector_norm_float(u), 9), 2.236067977)
	ASSERT(vector_norm_float_inf(u) == 2.)

	vector_free_float(u);
	return (1);
}

int matrix(void)
{
	return (1);
}

int subject(void)
{
	return vector() && matrix();
}

int main(void)
{
	return !subject();
}
