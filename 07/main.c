/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: selver <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/12 12:23:51 by selver            #+#    #+#             */
/*   Updated: 2022/03/28 09:52:41 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include.h"
//debile

double roundd(double n, double i)
{
    return floor(pow(10,i)*n)/pow(10,i);
}

int vector(void)
{
	t_matrix_float u = matrix_create_from_literal(float, 2, {
			{1., 0.},
			{0., 1.}}
			);
	t_vector_float v = vector_create_from_literal(float, {4., 2.});

	PRESENT_MATRIX(float, u);
	PRESENT_VECTOR(float, v);

	ASSERT_VECTOR(float, vector_mult_float_matrix_float(u, v), {4., 2.});

	matrix_free_float(u);

	u = matrix_create_from_literal(float, 2, {
			{2., 0.},
			{0., 2.}}
			);

	PRESENT_MATRIX(float, u);
	PRESENT_VECTOR(float, v);

	ASSERT_VECTOR(float, vector_mult_float_matrix_float(u, v), {8., 4.});

	matrix_free_float(u);

	u = matrix_create_from_literal(float, 2, {
			{2., -2.},
			{-2., 2.}}
			);

	PRESENT_MATRIX(float, u);
	PRESENT_VECTOR(float, v);

	ASSERT_VECTOR(float, vector_mult_float_matrix_float(u, v), {4., -4.});

	matrix_free_float(u);
	vector_free_float(v);
	return (1);
}

int matrix(void)
{
	t_matrix_float u = matrix_create_from_literal(float, 2, {
			{1., 0.},
			{0., 1.}}
			);
	t_matrix_float v = matrix_create_from_literal(float, 2, {
			{1., 0.},
			{0., 1.}}
			);

	PRESENT_MATRIX(float, u);
	PRESENT_MATRIX(float, v);

	ASSERT_MATRIX(float, matrix_mult_float(u, v), 2, { {1., 0.}, {0., 1.}});

	matrix_free_float(v);
	v = matrix_create_from_literal(float, 2, {
			{2., 1.},
			{4., 2.}}
			);

	PRESENT_MATRIX(float, u);
	PRESENT_MATRIX(float, v);

	ASSERT_MATRIX(float, matrix_mult_float(u, v), 2, { {2., 1.}, {4., 2.}});

	matrix_free_float(u);
	u = matrix_create_from_literal(float, 2, {
			{3., -5.},
			{6., 8.}}
			);

	PRESENT_MATRIX(float, u);
	PRESENT_MATRIX(float, v);

	ASSERT_MATRIX(float, matrix_mult_float(u, v), 2, { {-14., -7.}, {44., 22.}});

	matrix_free_float(u);
	return (1);
}

int subject(void)
{
	return vector() && matrix();
}

int main(void)
{
	return !subject();
}
