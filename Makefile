SHELL = /bin/bash
TARGETS=ex00 ex01 ex02 ex03 ex04 ex05 ex06 ex07 ex08 ex09 ex10 ex11 ex12 ex13 ex14 ex15
DIRS=00 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15
TARGETS=$(addprefix ex, $(DIRS))
DEL=/bin/rm -f
PRINT=/bin/echo
INCLUDE=matrix.h vector.h test.h other.h common.h
INCS=$(addprefix inc/, $(INCLUDE))

all : $(TARGETS)
	@echo "Everything done"

%/main.o: %/main.c $(INCS)
	echo "IN TMP RULE" $@
	export tmp=$@; make -C $${tmp%/main.o}

ex%: %/main.o
	@export tmp=$@; cp $${tmp#ex}/$@ .
	@echo Done	$@

fclean : 
	@$(DEL) $(TARGETS)
	@$(DEL) */main.o
	@$(PRINT) "Executable destroyed"
	@echo make\ -C\ {00..15}\ fclean\; | bash 

re : fclean all

help :
	@$(PRINT) "Rules available : all, fclean, re, ex[0-15] and help"

.PHONY : $(PHONY)

