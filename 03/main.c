/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: selver <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/12 12:23:51 by selver            #+#    #+#             */
/*   Updated: 2022/03/19 10:13:51 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include.h"

int vector(void)
{
	t_vector_float u = vector_create_from_literal(float, {0., 0.});
	t_vector_float v = vector_create_from_literal(float, {1., 1.});

	PRESENT_VECTOR(float, u);
	PRESENT_VECTOR(float, v);

	ASSERT(vector_dot_product_float(u, v) == 0.)

	vector_free_float(u);
	u = vector_create_from_literal(float, {1., 1.});

	PRESENT_VECTOR(float, u);
	PRESENT_VECTOR(float, v);

	ASSERT(vector_dot_product_float(u, v) == 2.)

	vector_free_float(u);
	vector_free_float(v);

	u = vector_create_from_literal(float, {-1., 6.});
	v = vector_create_from_literal(float, {3., 2.});

	PRESENT_VECTOR(float, u);
	PRESENT_VECTOR(float, v);

	ASSERT(vector_dot_product_float(u, v) == 9.)

	vector_free_float(u);
	vector_free_float(v);
	return (1);
}

int matrix(void)
{
	return (1);
}

int subject(void)
{
	return vector() && matrix();
}

int main(void)
{
	return !subject();
}
