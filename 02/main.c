/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: selver <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/12 12:23:51 by selver            #+#    #+#             */
/*   Updated: 2022/03/19 09:45:21 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include.h"

int normal(void)
{
	printf("%f\n", lerp_float(0., 1., 0.));
	printf("%f\n", lerp_float(0., 1., 1.));
	printf("%f\n", lerp_float(0., 1., 0.5));
	printf("%f\n", lerp_float(21., 42., 0.3));
}

int vector(void)
{
	t_vector_float u = vector_create_from_literal(float, {0.});
	t_vector_float v = vector_create_from_literal(float, {1.});

	PRESENT_VECTOR(float, u);
	PRESENT_VECTOR(float, v);

	ASSERT_VECTOR(float, vector_lerp_float_float(u, v, 1.), {1.})
	ASSERT_VECTOR(float, vector_lerp_float_float(u, v, 0.), {0.})
	ASSERT_VECTOR(float, vector_lerp_float_float(u, v, 0.5), {0.5})

	vector_free_float(u);
	vector_free_float(v);

	u = vector_create_from_literal(float, {21.});
	v = vector_create_from_literal(float, {42.});
	PRESENT_VECTOR(float, u);
	PRESENT_VECTOR(float, v);

	ASSERT_VECTOR(float, vector_lerp_float_float(u, v, 0.3), {27.3})

	vector_free_float(u);
	vector_free_float(v);

	u = vector_create_from_literal(float, {2., 1.});
	v = vector_create_from_literal(float, {4., 2.});

	PRESENT_VECTOR(float, u);
	PRESENT_VECTOR(float, v);

	ASSERT_VECTOR(float, vector_lerp_float_float(u, v, 0.3), {2.6, 1.3})

	vector_free_float(u);
	vector_free_float(v);
	return (1);
}

int matrix(void)
{
	t_matrix_float u = matrix_create_from_literal(float, 2, {{2.0, 1.0}, {3.0, 4.0}});
	t_matrix_float v = matrix_create_from_literal(float, 2, {{20.0, 10.0}, {30, 40}});

	PRESENT_MATRIX(float, u);
	PRESENT_MATRIX(float, v);

	ASSERT_MATRIX(float, matrix_lerp_float_float(u, v, 0.), 2, {{2., 1.}, {3., 4}})
	ASSERT_MATRIX(float, matrix_lerp_float_float(u, v, 0.5), 2, {{11., 5.5}, {16.5, 22}})
	ASSERT_MATRIX(float, matrix_lerp_float_float(u, v, 1.), 2, {{20., 10}, {30, 40}})

	matrix_free_float(u);
	matrix_free_float(v);

	return (1);
}

int subject(void)
{
	normal();
	return vector() && matrix();
}

int main(void)
{
	return !subject();
}
