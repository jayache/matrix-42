/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: selver <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/12 12:23:51 by selver            #+#    #+#             */
/*   Updated: 2022/03/17 09:58:13 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include.h"

int vector(void)
{
	t_vector_float e1 = vector_create_from_literal(float, {1., 0., 0.});
	t_vector_float e2 = vector_create_from_literal(float, {0., 1., 0.});
	t_vector_float e3 = vector_create_from_literal(float, {0., 0., 1.});

	t_vector_float v1 = vector_create_from_literal(float, {1., 2., 3.});
	t_vector_float v2 = vector_create_from_literal(float, {0., 10., -100.});

	t_vector_float e[] = {e1, e2, e3};
	t_vector_float v[] = {v1, v2};

	PRESENT_VECTOR(float, e1);
	PRESENT_VECTOR(float, e2);
	PRESENT_VECTOR(float, e3);

	ASSERT_VECTOR(float, vector_linear_combination_float_float(3, e, ((float[]){10., -2., 0.5})), {10, -2, 0.5})

	PRESENT_VECTOR(float, v1);
	PRESENT_VECTOR(float, v2);

	ASSERT_VECTOR(float, vector_linear_combination_float_float(2, v, ((float[]){10., -2.})), {10, 0., 230.})

	vector_free_float(e1);
	vector_free_float(e2);
	vector_free_float(e3);
	vector_free_float(v1);
	vector_free_float(v2);
	return (1);
}

int matrix(void)
{
	t_matrix_float u = matrix_create_from_literal(float, 2, {{1.0, 2.0}, {3.0, 4.0}});
	t_matrix_float v = matrix_create_from_literal(float, 2, {{7.0, 4.0}, {-2, 2}});

	t_matrix_float uu[] = {u, v};
	PRESENT_MATRIX(float, u);
	PRESENT_MATRIX(float, v);

	ASSERT_MATRIX(float, matrix_linear_combination_float_float(2, uu, ((float[]){10., -2.})), 2, {{-4, 12}, {34., 36}})

	matrix_free_float(u);
	matrix_free_float(v);

	return (1);
}

int subject(void)
{
	vector() && matrix();
}

int main(void)
{
	return !subject();
}
